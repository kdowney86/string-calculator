Feature: Add method
  A string calculator that can take string
  inputs using the Add method and returns an
  integer.

  Scenario: Add two numbers to form a sum in the string calculator
    Given an input of "1,2"
    When you run Add
    Then the result will be 3

  Scenario:
    Given an input of "1"
    When you run Add
    Then the result will be 1

  Scenario: Empty string in string calculator
    Given an input of ""
    When you run Add
    Then the result will be 0

  Scenario: Input 0 string in string calculator
    Given an input of "0"
    When you run Add
    Then the result will be 0

  Scenario: String calculator can handle an unknown amount of numbers
    Given an input of "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20"
    When you run Add
    Then the result will be 210

  Scenario: String calculator can handle new lines between numbers (instead of commas)
    Given an input of "1\n2,3"
    When you run Add
    Then the result will be 6

  Scenario: Support different delimiters
    #Delimiters are specified after // and followed by a new line
    Given an input of "//;\n1;2"
    When you run Add
    Then the result will be 3

  Scenario: The string calculator should ignore numbers bigger than 1000
    Given an input of "2,1001"
    When you run Add
    Then the result will be 2

  Scenario: String calculator delimiters can be of any length
    Given an input of "//[***]\n1***2***3"
    When you run Add
    Then the result will be 6

  Scenario: The string calculator supports multiple delimiters
    Given an input of "//[*][%]\n1*2%3"
    When you run Add
    Then the result will be 6

  Scenario Outline: All cases
    Given an input of <input>
    When you run Add
    Then the result will be <result>
    Examples:
      | input | result |
      | "1,2" | 3 |
      | "1" | 1 |
      | "" | 0 |
      | "0" | 0 |
      | "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20" | 210 |
      | "1\n2,3" | 6 |
      | "//;\n1;2" | 3 |
      | "2,1001" | 2 |
      | "//[***]\n1***2***3" | 6 |
      | "//[*][%]\n1*2%3" | 6 |