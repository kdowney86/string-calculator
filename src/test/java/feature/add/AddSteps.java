package feature.add;

import com.stringcalculator.calculator.AdditionCalculator;
import com.stringcalculator.exception.NegativeNumberException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 * Created by kelvin on 15/06/17.
 */
public class AddSteps {

    private String input;
    private Integer result;
    private AdditionCalculator additionCalculator = new AdditionCalculator();

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    private void setUp() {
        input = "";
        result = null;
    }

    @Given("^an input of \"([^\"]*)\"$")
    public void an_input_of(String input) throws Throwable {
        this.input = input;
    }

    @When("^you run Add$")
    public void you_run_Add() throws Throwable {
        result = additionCalculator.process(input);
    }

    @Then("^the result will be (\\d+)$")
    public void the_result_will_be(Integer expected) throws Throwable {
        Assert.assertEquals(expected, result);
    }
}
