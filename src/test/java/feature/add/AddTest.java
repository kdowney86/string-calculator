package feature.add;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;

/**4
 * Created by kelvin on 15/06/17.
 */
@RunWith(Cucumber.class)
@CucumberOptions(strict = false, features = "src/test/resources/feature/add", format = { "pretty",
        "html:target/site/cucumber-pretty",
        "json:target/cucumber.json" }, tags = { "~@ignore" })
public class AddTest {

}
