package com.stringcalculator;

/**
 * Created by kelvin on 21/06/17.
 */
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StringCalculatorApplicationTest {

    @Test
    public void contextLoads() {
    }

}
