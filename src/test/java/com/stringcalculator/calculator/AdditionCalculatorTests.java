package com.stringcalculator.calculator;

import com.stringcalculator.exception.NegativeNumberException;
import org.junit.Assert;
import org.junit.Test;

public class AdditionCalculatorTests {

	private AdditionCalculator additionCalculator = new AdditionCalculator();

	@Test
	public void testAddTwoCommaSeparatedValues() throws NegativeNumberException {
		//given
		String input = "1,2";
		Integer expected = 3;
		//when
		Integer result = additionCalculator.process(input);

		//then
		Assert.assertEquals(expected, result);
	}

	@Test
	public void testAddOneValue() throws NegativeNumberException {
		//given
		String input = "1";
		Integer expected = 1;

		//when
		Integer result = additionCalculator.process(input);

		//then
		Assert.assertEquals(expected, result);
	}

	@Test
	public void testAddEmptyString() throws NegativeNumberException {
		//given
		String input = "";
		Integer expected = 0;

		//when
		Integer result = additionCalculator.process(input);

		//then
		Assert.assertEquals(expected, result);
	}

	@Test
	public void testAdd0() throws NegativeNumberException {
		//given
		String input = "0";
		Integer expected = 0;

		//when
		Integer result = additionCalculator.process(input);

		//then
		Assert.assertEquals(expected, result);
	}

	@Test
	public void testAdd20CommaSeparatedValues() throws NegativeNumberException {
		//given
		String input = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20";
		Integer expected = 210;

		//when
		Integer result = additionCalculator.process(input);

		//then
		Assert.assertEquals(expected, result);
	}

	@Test
	public void testHandleNewLines() throws NegativeNumberException {
		//given
		String input = "1\n2,3";
		Integer expected = 6;

		//when
		Integer result = additionCalculator.process(input);

		//then
		Assert.assertEquals(expected, result);
	}

	@Test
	public void testHandleSpecifiedDelimiters() throws NegativeNumberException {
		//given
		String input = "//;\n1;2";
		Integer expected = 3;

		//when
		Integer result = additionCalculator.process(input);

		//then
		Assert.assertEquals(expected, result);
		}

	@Test
	public void testIgnoreNumbersGreaterThan1000() throws NegativeNumberException {
		//given
		String input = "2,1001";
		Integer expected = 2;

		//when
		Integer result = additionCalculator.process(input);

		//then
		Assert.assertEquals(expected, result);
	}

	@Test
	public void testCustomerDelimitersInBrackets() throws NegativeNumberException {
		//given
		String input = "//[***]\\n1***2***3";
		Integer expected = 6;

		//when
		Integer result = additionCalculator.process(input);

		//then
		Assert.assertEquals(expected, result);
	}

	@Test
	public void testMultipleCustomDelimiters() throws NegativeNumberException {
		//given
		String input = "//[*][%]\\n1*2%3";
		Integer expected = 6;

		//when
		Integer result = additionCalculator.process(input);

		//then
		Assert.assertEquals(expected, result);
	}

	@Test(expected = NegativeNumberException.class)
	public void testNegativeNumbers() throws NegativeNumberException {
		//given
		String input = "-10,-10";

		//when
		additionCalculator.process(input);
	}

}
