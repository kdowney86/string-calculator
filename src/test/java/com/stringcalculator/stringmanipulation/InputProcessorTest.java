package com.stringcalculator.stringmanipulation;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kelvin on 21/06/17.
 */
public class InputProcessorTest {

    private InputProcessor inputProcessor = new InputProcessor();

    private String EMPTY_STRING = "";


    @Test
    public void testProcessEmptyInput() {
        //given
        String input = EMPTY_STRING;

        //when
        String[] result = inputProcessor.process(input);

        //then
        Assert.assertEquals(result[0], EMPTY_STRING);
    }

    @Test
    public void testProcessSingleIntegerInput() {
        //given
        String input = "1";

        //when
        String[] result = inputProcessor.process(input);

        //then
        String [] expected = {"1"};
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testProcessTwoCommaSeparatedIntegers() {
        //given
        String input = "1,2";

        //when
        String[] result = inputProcessor.process(input);

        //then
        String [] expected = {"1", "2"};
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testProcessThreeCommaSeparatedIntegers() {
        //given
        String input = "1,3,6";

        //when
        String[] result = inputProcessor.process(input);

        //then
        String [] expected = {"1", "3", "6"};
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testProcessThreeNewLineSeparatedIntegers() {
        //given
        String input = "1\n3\n6";

        //when
        String[] result = inputProcessor.process(input);

        //then
        String [] expected = {"1", "3", "6"};
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testProcessCustomDelimiter() {
        //given
        String input = "//;\\n1;2";

        //when
        String[] result = inputProcessor.process(input);

        //then
        String [] expected = {"1", "2"};
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testProcessCustomDelimiterWithBrackets() {
        //given
        String input = "//[***]\\n1***2***3";

        //when
        String[] result = inputProcessor.process(input);

        //then
        String [] expected = {"1", "2" ,"3"};
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testProcessMultipleCustomDelimitersWithBrackets() {
        //given
        String input = "//[*][%]\\n1*2%3";

        //when
        String[] result = inputProcessor.process(input);

        //then
        String [] expected = {"1", "2" ,"3"};
        Assert.assertEquals(expected, result);
    }
}
