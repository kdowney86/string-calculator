package com.stringcalculator.controller;

import com.stringcalculator.calculator.AdditionCalculator;
import com.stringcalculator.exception.NegativeNumberException;
import com.stringcalculator.model.CalculatorInput;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * Created by kelvin on 19/06/17.
 */
@Controller
public class StringCalculatorController {

    // inject via application.properties
    @Value("${calculator.message:test}")
    private String result = "";

    private AdditionCalculator additionCalculator;

    @RequestMapping("/")
    public String calculator(@ModelAttribute("calculatorInput") CalculatorInput calculatorInput, Map<String, Object> model) {
        return "calculator";
    }

    @RequestMapping("/add")
    public String add(@ModelAttribute("calculatorInput") CalculatorInput calculatorInput, Map<String, Object> model) throws NegativeNumberException {
        Integer resultInteger = additionCalculator.process(calculatorInput.getValue());
        result = Integer.toString(resultInteger);
        model.put("result", this.result);
        return "result";
    }
}
