package com.stringcalculator.calculator;

import com.stringcalculator.exception.NegativeNumberException;
import com.stringcalculator.stringmanipulation.InputProcessor;

/**
 * Created by kelvin on 19/06/17.
 */
public class AdditionCalculator {

    private static final int MAX_INT = 1000;
    private InputProcessor inputProcessor = new InputProcessor();

    public Integer process(String input) throws NegativeNumberException {
        Integer result;
        String[] processedInput = inputProcessor.process(input);
        result = add(processedInput);
        return result;
    }

    private Integer add(String [] inputs) throws NegativeNumberException {
        Integer result = 0;

        for(String value: inputs) {
            if (value.length() > 0 && Integer.parseInt(value) < 0) throw new NegativeNumberException("Inputs must be positive integers");
            if (value.length() > 0 && Integer.parseInt(value) < MAX_INT) result += Integer.parseInt(value);
        }
        return result;
    }

}
