package com.stringcalculator.exception;

/**
 * Created by kelvin on 23/06/17.
 */
public class NegativeNumberException extends Exception{
    public NegativeNumberException(String message) {
        super(message);
    }
}
