package com.stringcalculator.model;

/**
 * Created by kelvin on 19/06/17.
 */
public class CalculatorInput {

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
