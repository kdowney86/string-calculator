package com.stringcalculator.stringmanipulation;


import com.stringcalculator.constants.StringCalculatorConstants;

/**
 * Created by kelvin on 21/06/17.
 */
public class InputProcessor {

    public String sanitiseInput(String input, String[] customDelimiters) {
        String sanitisedInput = input;

        sanitisedInput = removeCustomDelimiters(sanitisedInput, customDelimiters);
        sanitisedInput = replaceNewLinesWithDelimiter(sanitisedInput);
        return sanitisedInput;
    }

    private String replaceNewLinesWithDelimiter(String input) {
        String sanitisedInput = input.replace(StringCalculatorConstants.NEW_LINE_ALTERNATE, StringCalculatorConstants.DELIMITER);
        sanitisedInput = sanitisedInput.replace(StringCalculatorConstants.NEW_LINE, StringCalculatorConstants.DELIMITER);
        return sanitisedInput;
    }

    private String removeCustomDelimiters(String input, String[] customDelimiters) {
        if (hasNoValidCustomDelimiters(customDelimiters)) return input;

        String sanitisedInput = input.replace(StringCalculatorConstants.NEW_LINE_ALTERNATE, StringCalculatorConstants.NEW_LINE);
        int indexOfNewLine = getIndexOfNewLine(sanitisedInput);
        int cutOffPoint = indexOfNewLine + StringCalculatorConstants.NEW_LINE.length();

        sanitisedInput = sanitisedInput.substring(cutOffPoint);

        for (String customDelimiter: customDelimiters) {
           sanitisedInput = sanitisedInput.replace(customDelimiter, StringCalculatorConstants.DELIMITER);
        }

        return sanitisedInput;
    }

    private int getIndexOfNewLine(String input) {
        return input.indexOf(StringCalculatorConstants.NEW_LINE);
    }

    private boolean hasNoValidCustomDelimiters(String[] customDelimiters) {
        return customDelimiters.length == 0 || (customDelimiters.length == 1 && customDelimiters[0].equals(StringCalculatorConstants.EMPTY_STRING));
    }

    public String [] process(String input) {
        String [] customDelimiters = getCustomDelimiters(input);
        String sanitisedInput = sanitiseInput(input, customDelimiters);
        return split(sanitisedInput);
    }

    public String [] split(String input) {
        return input.split(StringCalculatorConstants.DELIMITER);
    }

    public String[] getCustomDelimiters(String input) {
        String unparsedDelimiters = StringCalculatorConstants.EMPTY_STRING;
        String parsedInput = replaceNewLinesWithDelimiter(input);
        if (parsedInput.startsWith(StringCalculatorConstants.CUSTOM_DELIMITER_SIGNIFIER)) {
            int indexOfNewLine = parsedInput.indexOf(StringCalculatorConstants.DELIMITER);
            unparsedDelimiters = parsedInput.substring(StringCalculatorConstants.CUSTOM_DELIMITER_SIGNIFIER.length(), indexOfNewLine);
        }
        if (delimiterHasBrackets(unparsedDelimiters)) {
            unparsedDelimiters = unparsedDelimiters.substring(1, unparsedDelimiters.length() -1);
        }

        String delimiterSeparator = "\\" + StringCalculatorConstants.CUSTOM_DELIMITER_CLOSING_BRACKET + "\\" +  StringCalculatorConstants.CUSTOM_DELIMITER_OPENING_BRACKET;
        return unparsedDelimiters.split(delimiterSeparator);
    }

    private boolean delimiterHasBrackets(String customDelimiter) {
        return customDelimiter.startsWith(StringCalculatorConstants.CUSTOM_DELIMITER_OPENING_BRACKET)
                && customDelimiter.endsWith(StringCalculatorConstants.CUSTOM_DELIMITER_CLOSING_BRACKET);
    }
}
