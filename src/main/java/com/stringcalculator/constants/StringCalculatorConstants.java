package com.stringcalculator.constants;

/**
 * Created by kelvin on 21/06/17.
 */
public class StringCalculatorConstants {

    private StringCalculatorConstants() {

    }

    public static final String DELIMITER = ",";
    public static final String NEW_LINE = "\n";
    public static final String NEW_LINE_ALTERNATE = "\\n";
    public static final String CUSTOM_DELIMITER_SIGNIFIER = "//";
    public static final String EMPTY_STRING = "";
    public static final String CUSTOM_DELIMITER_OPENING_BRACKET = "[";
    public static final String CUSTOM_DELIMITER_CLOSING_BRACKET = "]";

}
